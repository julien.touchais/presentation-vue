import { makeProject } from "@motion-canvas/core";

import htmlCssScene from "./scenes/htmlCssScene?scene";
import JsScene from "./scenes/JsScene?scene";

export default makeProject({
  scenes: [JsScene],
});
