---
transition: slide-left
layout: center
title: "Historique : intro"
clicks: 0
---

<div class="text-center -mt-16">
  
  # Comment créer un site web
  
  ###### <span class="!text-xl">Petit historique...</span>

</div>

<div w="2/3" absolute top="2/3" right-0 h-2 class="bg-primary" rounded-l></div>
<div w-10 left="[calc(100%/3-2.5rem/2)]" absolute bottom="[calc(100%/3-3rem/2)]" rounded-full h-10 class="bg-primary"></div>
<LightOrDark>
<template #light>
<div w-7 left="[calc(100%/3-1.75rem/2)]" absolute bottom="[calc(100%/3-2.25rem/2)]" rounded-full h-7 class="bg-white"></div>
</template>
<template #dark>
<div w-7 left="[calc(100%/3-1.75rem/2)]" absolute bottom="[calc(100%/3-2.25rem/2)]" rounded-full h-7 class="bg-[var(--slidev-slide-container-background,black)]"></div>
</template>
</LightOrDark>
<div w-4 left="[calc(100%/3-1rem/2)]" absolute bottom="[calc(100%/3-1.5rem/2)]" rounded-full h-4 class="bg-primary"></div>

<!--
Avant tout, un **petit retour semi-historique**, **semi-technique** sur la **conception** des sites web :
-->

---
transition: slide-left
title: "Historique"
clicks: 3
---

<div absolute top="2/3" left-0 h-2 class="w-1/10 bg-primary historique-ligne"></div>
<div w-7 left="[calc(100%/10-1.75rem/2)]" absolute bottom="[calc(100%/3-2.25rem/2)]" rounded-full h-7 class="bg-primary"></div>
<LightOrDark>
<template #light>
<div w-4 z-10 left="[calc(100%/10-1rem/2)]" absolute bottom="[calc(100%/3-1.5rem/2)]" rounded-full h-4 class="bg-white"></div>
</template>
<template #dark>
<div w-4 z-10 left="[calc(100%/10-1rem/2)]" absolute bottom="[calc(100%/3-1.5rem/2)]" rounded-full h-4 class="bg-[rgb(18_18_18)]"></div>
</template>
</LightOrDark>
<div w-2 absolute top="[calc(2*100%/3+1.5rem)]" left="[calc(100%/10-.5rem/2)]" h="1/20" rounded class="bg-primary"></div>

  <div v-click="[0,1]" mb-4 class="historique-element-title historique-element-title-bottom absolute top-[calc(2*100%/3+4rem)] left-14">
    
  # HTML <simple-icons-html5 class="text-orange-6" /> / CSS <simple-icons-css3 class="text-blue" />

Affichage de pages **statiques**

  </div>

  <div v-click="[0,1]" shadow rounded-xl class="bg-[#f3f4f6] img-wrapper absolute top-8 left-8 right-1/2">

![HTML and CSS working together to build a website](/images/1-historique/1-html-css.gif)

  </div>

<div v-click="[1,4]" rounded-full w="1/4" absolute top="2/3" left="1/10" h-2 class="bg-primary historique-ligne"></div>
<div v-click="[1,4]" w-7 left="[calc(3.5*100%/10-1.75rem/2)]" absolute bottom="[calc(100%/3-2.25rem/2)]" rounded-full h-7 class="bg-primary !delay-500 historique-marker-rond"></div>
<LightOrDark>
<template #light>
<div v-click="[1,4]" w-4 z-10 left="[calc(3.5*100%/10-1rem/2)]" absolute bottom="[calc(100%/3-1.5rem/2)]" rounded-full h-4 class="bg-white !delay-500 historique-marker-rond"></div>
</template>
<template #dark>
<div v-click="[1,4]" w-4 z-10 left="[calc(3.5*100%/10-1rem/2)]" absolute bottom="[calc(100%/3-1.5rem/2)]" rounded-full h-4 class="bg-[rgb(18_18_18)] !delay-500 historique-marker-rond"></div>
</template>
</LightOrDark>
<div v-click="[1,4]" w-2 absolute top="[calc(2*100%/3+1.5rem)]" left="[calc(3.5*100%/10-.5rem/2)]" h="1/20" rounded class="bg-primary !delay-700 historique-marker-ligne historique-marker-ligne-bottom"></div>

  <div v-click="[1,2]" mb-4 class="historique-element-title historique-element-title-bottom absolute top-[calc(2*100%/3+4rem)] left-72">


<div v-click="[1,4]" delay-1000 duration-500>
  
  # Javascript <logos-javascript />
  
  On rajoute un peu d'**interactivité**
</div>

  </div>

  <div v-click="[1,2]" shadow rounded-xl overflow-hidden class="img-wrapper absolute top-8 left-8 right-1/2">

![Mouse selecting colors in a dropdown](/images/1-historique/2-js.gif)

  </div>


<div v-click="[2,4]" w="1/4" absolute top="2/3" left="7/20" h-2 class="bg-primary historique-ligne"></div>
<div v-click="[2,4]" w-7 left="[calc(6*100%/10-1.75rem/2)]" absolute bottom="[calc(100%/3-2.25rem/2)]" rounded-full h-7 class="bg-primary !delay-500 historique-marker-rond"></div>
<LightOrDark>
<template #light>
<div v-click="[2,4]" w-4 z-10 left="[calc(6*100%/10-1rem/2)]" absolute bottom="[calc(100%/3-1.5rem/2)]" rounded-full h-4 class="bg-white !delay-500 historique-marker-rond"></div>
</template>
<template #dark>
<div v-click="[2,4]" w-4 z-10 left="[calc(6*100%/10-1rem/2)]" absolute bottom="[calc(100%/3-1.5rem/2)]" rounded-full h-4 class="bg-[rgb(18_18_18)] !delay-500 historique-marker-rond"></div>
</template>
</LightOrDark>
<div v-click="[2,4]" w-2 absolute top="[calc(2*100%/3+1.5rem)]" left="[calc(6*100%/10-.5rem/2)]" h="1/20" rounded class="bg-primary !delay-700 historique-marker-ligne historique-marker-ligne-bottom"></div>

<!-- <div grid="~ cols-2 items-end" h-full pb-48> -->

  <!-- <div mb-4 pr-4> -->

  <div v-click="[2,3]" mb-4 class="historique-element-title historique-element-title-bottom absolute  top-[calc(2*100%/3+4rem)] left-132">


<div v-click="[2,4]" delay-1000 duration-500>

# Bibliothèques <emojione-v1-books />

Pour **manipuler le document** (HTML) plus facilement (_ex : <skill-icons-jquery /> jQuery_)
</div>

  </div>

  <div v-click="[2,3]" shadow rounded-xl overflow-hidden class="img-wrapper absolute top-24 left-8 right-1/2">

![Libs](/images/1-historique/3-biblios.gif)

  </div>

<div v-click="[3,4]" w="1/4" absolute top="2/3" left="3/5" h-2 class="bg-primary historique-ligne"></div>
<div v-click="[3,4]" w-10 left="[calc(8.5*100%/10-2.5rem/2)]" absolute bottom="[calc(100%/3-3rem/2)]" rounded-full h-10 class="bg-primary !delay-500 historique-marker-rond"></div>
<LightOrDark>
<template #light>
<div v-click="[3,4]" w-7 z-10 left="[calc(8.5*100%/10-1.75rem/2)]" absolute bottom="[calc(100%/3-2.25rem/2)]" rounded-full h-7 class="bg-white !delay-500 historique-marker-rond"></div>
</template>
<template #dark>
<div v-click="[3,4]" w-7 z-10 left="[calc(8.5*100%/10-1.75rem/2)]" absolute bottom="[calc(100%/3-2.25rem/2)]" rounded-full h-7 class="bg-[var(--slidev-slide-container-background,black)] !delay-500 historique-marker-rond"></div>
</template>
</LightOrDark>
<div v-click="[3,4]" w-4 z-10 left="[calc(8.5*100%/10-1rem/2)]" absolute bottom="[calc(100%/3-1.5rem/2)]" rounded-full h-4 class="bg-primary !delay-500 historique-marker-rond"></div>
<div v-click="[3,4]" w-2 absolute bottom="[calc(100%/3+2rem)]" left="[calc(8.5*100%/10-.5rem/2)]" h="1/20" rounded class="bg-primary !delay-700 historique-marker-ligne historique-marker-ligne-top"></div>

<!-- <div grid="~ cols-2 items-end" h-full pb-48> -->

  <!-- <div mb-4 pr-4> -->

  <div v-click="[3,4]" mb-4 class="historique-element-title historique-element-title-top absolute bottom-[calc(100%/3+3rem)] left-140">


<div v-click="[3,4]" delay-1000 duration-500>

# Frameworks <noto-toolbox /> <span class="text-sm italic text-slate-500">(ouf, on y est !)</span>

**Boîtes à outils** complètes pour **encadrer** le développement des sites (_ex : <logos-vue /> <span class="font-900 underline">Vue.js</span> !_)
</div>

  </div>

  <div v-click="[3,4]" shadow rounded-xl overflow-hidden class="img-wrapper absolute top-8 left-8 right-[calc(100%/2+4rem)]">

  <logos-vue absolute top-18 left-47 class="skew-x-8 text-[2.5rem]"/>
  <noto-wrench absolute top-34 left-52 class="rotate-[-140deg] text-[2.5rem]"/>
  <noto-toolbox absolute top-32 left-39 class="scale-x-90 skew-y-4 text-[2.5rem]"/>

![Super-vue with its tools](/images/1-historique/4-superhero.gif)

  </div>

<!-- 
- On commence avec **HTML et CSS** : **HTML** permet de définir le **contenu des pages**, et **CSS** de le **mettre en forme**, mais tout est **défini à l'avance**, on a des sites qui ne sont **pas interactifs** ;
- Pour **palier** à cela, on rajoute le **JavaScript**, qui lui justement permet de **réagir aux actions de l'utilisateur** et de **modifier la page en conséquence** ;
- Avec le temps, les sites se **complexifient**, donc des **bibliothèques logicielles** qui permettent d'**interagir plus facilement** avec le document apparaissent et **deviennent très populaire** ;
- Et enfin, on y arrive, les **bibliothèques** passent un cap et **évoluent** en **_frameworks_ ou cadriciels**, qui sont des **outils complets** qui définissent des moyens standards pour **encadrer le développement des sites web** : c'est dans cette catégorie que tombe **Vue.js**.
-->
